package hw6;

public class AddCommand implements Command {
    private String word;
    
    public AddCommand(String word) {
        this.word = word;
    }

    @Override
    public void execute() {
        WordList.getInstance().addWord(word);
    }
    
    @Override
    public boolean canUndo() {
        return true;
    }
    
    @Override
    public boolean canRedo() {
        return true;
    }
    
    @Override
    public void redo() {
        execute();
    }
    
    @Override
    public void undo() {
        WordList.getInstance().removeWord(word);
    }
}
