package hw6;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class CommandInvokerTest {

    @Test
    void testInvokeCommand() {
        var os = new ByteArrayOutputStream();
        System.setOut(new PrintStream(os));
        
        CommandInvoker.getInstance().invokeCommand("invalid command", "");
        assertEquals(os.toString(), "Error: Invalid command\n");
    }

}
