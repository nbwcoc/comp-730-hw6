package hw6;

import java.util.HashSet;

public class WordList {
    private static WordList instance = new WordList();
    private HashSet<String> words = new HashSet<String>();
    
    private WordList() {}

    public static WordList getInstance() {
        return instance;
    }
    
    public void addWord(String word) {
        words.add(word);
    }
    
    public void removeWord(String word) {
        words.remove(word);
    }
    
    public String toString() {
        String rv = "";
        
        for (var v : words) {
            rv += v;
            rv += " ";
        }
    
        return rv;
    }
    
}
