package hw6;

public class QuitCommand implements Command {

    @Override
    public void execute() {
        RunHW6.signalExit();
    }
}
