package hw6;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class ErrorCommandTest {

    @Test
    void testExecute() {
        var os = new ByteArrayOutputStream();
        System.setOut(new PrintStream(os));
        
        new ErrorCommand("test").execute();
        
        assertEquals(os.toString(), "Error: test\n");
    }
    
    @Test
    void testDefaults() {
        var testObject = new ErrorCommand("");
        assertFalse(testObject.canRedo());
        assertFalse(testObject.canUndo());
    }
}
