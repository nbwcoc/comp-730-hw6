package hw6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DeleteCommandTest {

    @Test
    void testExecute() {
        WordList.getInstance().addWord("test");
        new DeleteCommand("test").execute();
        assertEquals(WordList.getInstance().toString(), "");
    }

    @Test
    void testRedo() {
        var testObject = new DeleteCommand("test");
        WordList.getInstance().addWord("test");
        testObject.execute();
        testObject.undo();
        testObject.redo();
        assertEquals(WordList.getInstance().toString(), "");
    }

    @Test
    void testUndo() {
        var testObject = new DeleteCommand("test");
        WordList.getInstance().addWord("test");
        testObject.execute();
        testObject.undo();
        assertEquals(WordList.getInstance().toString(), "test ");
    }

}
