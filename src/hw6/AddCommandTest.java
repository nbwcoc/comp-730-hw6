package hw6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AddCommandTest {

    @Test
    void testExecute() {
        new AddCommand("test").execute();
        assertEquals(WordList.getInstance().toString(), "test ");
    }
    
    @Test
    void testUndo() {
        var testObject = new AddCommand("test");
        testObject.execute();
        testObject.undo();
        assertEquals(WordList.getInstance().toString(), "");
    }
    
    @Test
    void testRedo() {
        var testObject = new AddCommand("test");
        testObject.execute();
        testObject.undo();
        testObject.redo();
        assertEquals(WordList.getInstance().toString(), "test ");
    }
}
