package hw6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class WordListTest {

    @Test
    void testAddWord() {
        var list = WordList.getInstance();
        list.addWord("test");
        assertEquals(list.toString(), "test ");
        list.addWord("test");
        assertEquals(list.toString(), "test ");
        list.addWord("test1");
        assertTrue(list.toString().equals("test test1 ") || list.toString().equals("test1 test "));
    }

    @Test
    void testRemoveWord() {
        var list = WordList.getInstance();
        list.addWord("test");
        list.removeWord("test1");
        list.removeWord("test");
        assertEquals(list.toString(), "");
    }

}
