package hw6;

import java.util.Scanner;

public class RunHW6 {
    private static boolean shouldExit = false;
    public static void signalExit() {
        shouldExit = true;
    }

    public static void main(String[] args) {
        var stdin = new Scanner(System.in);
        while (!shouldExit) {
            System.out.print(">");
            var line = stdin.nextLine();
            if (line.isEmpty())
                continue;
            var command = line.split(" ")[0];
            var cmd_args = line.replaceFirst("^.* ", "");
            CommandInvoker.getInstance().invokeCommand(command, cmd_args);
        }
        
        stdin.close();
    }
}
