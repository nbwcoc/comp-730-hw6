package hw6;

public interface Command {
    public default boolean canUndo() {
        return false;
    }
    
    public default boolean canRedo() {
        return false;
    }
    
    public void execute();
    
    public default void redo() {}
    public default void undo() {}
}
