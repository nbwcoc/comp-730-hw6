package hw6;

public class RedoCommand implements Command {

    @Override
    public void execute() {
        UndoStack.getInstance().popRedo().redo();
    }

}
