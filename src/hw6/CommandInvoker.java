package hw6;

import java.util.HashMap;
import java.util.function.Function;

public class CommandInvoker {
    private static CommandInvoker instance = new CommandInvoker();
    private CommandInvoker() {
        // I wanted to do a bunch of stuff with reflection, but that seemed
        // very fragile and hacky.
        commands.put("undo", (String x) -> {return new UndoCommand();});
        commands.put("redo", (String x) -> {return new RedoCommand();});
        commands.put("add", (String word) -> {return new AddCommand(word);});
        commands.put("delete", (String word) -> {return new DeleteCommand(word);});
        commands.put("quit", (String x) -> {return new QuitCommand();});
    }
    
    public static CommandInvoker getInstance() {
        return instance;
    }
    
    private HashMap<String, Function<String, Command>> commands = new HashMap<>();
    private ErrorCommand error = new ErrorCommand("Invalid command");
    
    public void invokeCommand(String command, String args) {
        var cmd = commands.get(command);
        
        if (cmd == null) {
            error.execute();
            return;
        }
        
        // This is fragile. There's no way in Java to guarantee that a subclass
        // will have a certain constructor.
        var concreteCmd = cmd.apply(args);
        
        concreteCmd.execute();
        UndoStack.getInstance().push(concreteCmd);
        System.out.println(WordList.getInstance());
    }
}
