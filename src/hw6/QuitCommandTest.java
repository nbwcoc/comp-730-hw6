package hw6;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class QuitCommandTest {

    @Test
    void testExecute() throws InterruptedException {
        var os = new ByteArrayOutputStream();
        System.setOut(new PrintStream(os));

        var in = new ByteArrayInputStream("quit\n".getBytes());
        System.setIn(in);
        
        Runnable mainRunner = () -> {RunHW6.main(null);};
        var mainThread = new Thread(mainRunner);
        mainThread.start();
        
        Thread.sleep(5000);
        
        assertEquals(mainThread.getState(), Thread.State.TERMINATED);
    }

}
