package hw6;

public class UndoCommand implements Command {
    
    @Override
    public void execute() {
        UndoStack.getInstance().popUndo().undo();
    }

}
