package hw6;

public class DeleteCommand implements Command {
    private String word;
    
    public DeleteCommand(String word) {
        this.word = word;
    }

    @Override
    public void execute() {
        WordList.getInstance().removeWord(word);
    }
    
    @Override
    public boolean canUndo() {
        return true;
    }
    
    @Override
    public boolean canRedo() {
        return true;
    }
    
    @Override
    public void redo() {
        execute();
    }
    
    @Override
    public void undo() {
        WordList.getInstance().addWord(word);
    }
}
