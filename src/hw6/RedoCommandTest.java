package hw6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RedoCommandTest {

    @Test
    void testExecute() {
        var invoker = CommandInvoker.getInstance();
        invoker.invokeCommand("add", "test");
        invoker.invokeCommand("undo", "");
        invoker.invokeCommand("redo", "");
        
        assertEquals(WordList.getInstance().toString(), "test ");
    }

}
