package hw6;

public class ErrorCommand implements Command {
    private String arg;
    
    public ErrorCommand(String args) {
        this.arg = args;
    }

    @Override
    public void execute() {
        System.out.printf("Error: %s\n", arg);
    }
}
