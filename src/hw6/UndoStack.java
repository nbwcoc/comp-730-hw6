package hw6;

import java.util.ArrayList;

public class UndoStack {
    private ArrayList<Command> stack = new ArrayList<>();
    private int i = -1;
    private static UndoStack instance = new UndoStack();
    
    private UndoStack() {}
    
    public static UndoStack getInstance() {
        return instance;
    }
    
    public Command popRedo() {
        if (i < -1 || i == stack.size() - 1 || stack.size() == 0)
            return new ErrorCommand("out of items to redo");
        
        return stack.get(++i);
    }
    
    public Command popUndo() {
        if (i < 0)
            return new ErrorCommand("out of items to undo");
        
        return stack.get(i--);
    }
    
    public void push(Command cmd) {
        if (cmd.canUndo()) {
            for (int j = i + 1; j < stack.size(); j++)
                stack.remove(j);
            
            stack.add(cmd);
            i++;
        }
    }
}
